/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Giovanni Bandeira
 */
public class Nao2GrauException extends java.lang.RuntimeException{

    public Nao2GrauException() {
        super(String.format("Coeficiente nao pode ser zero"));
    }

}
