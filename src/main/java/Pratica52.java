
import utfpr.ct.dainf.if62c.pratica.Equacao2Grau;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica52 {
    public static void main(String[] args) {
        Equacao2Grau eq1,eq2,eq3;
        Number raiz11,raiz21,raiz12,raiz22,raiz13,raiz23;
        eq1= new Equacao2Grau(1.,-4.,5.);
        eq2= new Equacao2Grau(4.,-4.,1.);
        eq3= new Equacao2Grau(1.,-5.,6.);
        raiz11=eq1.getRaiz1();
        raiz21=eq1.getRaiz2();
        raiz12=eq2.getRaiz1();
        raiz22=eq2.getRaiz2();
        raiz13=eq3.getRaiz1();
        raiz23=eq3.getRaiz2();
        
        System.out.println(String.format("As raizes da eq1 sao %d e %d",raiz11,raiz21));
        System.out.println(String.format("As raizes da eq2 sao %d e %d",raiz12,raiz22));
        System.out.println(String.format("As raizes da eq3 sao %d e %d",raiz13,raiz23));
        
    }
}
