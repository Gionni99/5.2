/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Giovanni Bandeira
 */
public class Equacao2Grau {
    Number a,b,c;

    public Equacao2Grau(Number a, Number b, Number c) {
        if((int)a == 0)
        {
            throw new Nao2GrauException();
        }
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public Number getA() {
        return a;
    }

    public void setA(Number a) {
        if((int)a == 0)
        {
            throw new Nao2GrauException();
        }
        this.a = a;
    }

    public Number getB() {
        return b;
    }

    public void setB(Number b) {
        if((int)b == 0)
        {
            throw new Nao2GrauException();
        }
        this.b = b;
    }

    public Number getC() {
        return c;
    }

    public void setC(Number c) {
        if((int)c == 0)
        {
            throw new Nao2GrauException();
        }
        this.c = c;
    }
    
    public Number getRaiz1() {
        Number x;
        if(Math.pow((double) this.b,2)-4.*(double)this.a*(double)this.c<0)
        {
            throw new SemRaizException();
        }
        x=((double)this.b-Math.sqrt(Math.pow((double) this.b,2)-4.*(double)this.a*(double)this.c)/(2*(double)this.a));
        return x;
    }
    public Number getRaiz2() {
        Number x;
        if(Math.pow((double) this.b,2)-4.*(double)this.a*(double)this.c<0)
        {
            throw new SemRaizException();
        }
        x=((double)this.b+Math.sqrt(Math.pow((double) this.b,2)-4.*(double)this.a*(double)this.c)/(2*(double)this.a));
        return x;
    }
}
